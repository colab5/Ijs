# Javascript Recources

## Books

- [Eloquent Javascript](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/Ko4xUBKY)
- [Full Stack Javascript](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/ahgzSDgB)
- [Javascript & Jquery](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/nw5FlTSb)
- [Javascript The Difinitive Guide](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/W4wlgTqB)
- [Practical Node.js](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/CghRXL5D)
- [Scaling your Node.js Apps](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/S5g3STzA)

## Videos

### Beginners

- [Learn Javascript - Full Course for Beginners](https://www.youtube.com/watch?v=PkZNo7MFNFg)
- [Khan Academy Tutorials](https://www.youtube.com/watch?v=FCMxA3m_Imc&list=PLC51FJvpvRvxAdEO8t6mLXt2m0UMxZrnI)
- [Javascript Crash Course](https://www.youtube.com/watch?v=hdI2bqOjy3c)

### Novice

- [Javascript 8 hour course](https://www.youtube.com/watch?v=Qqx_wzMmFeA)
- [Make a Music App](https://www.youtube.com/watch?v=2VJlzeEVL8A)
- [Full Javascript Course - 4Hrs](https://www.youtube.com/watch?v=dOnAC2Rr-6A)

### Advanced

- [Javascript Fundamentals](https://www.youtube.com/watch?v=2Ji-clqUYnA)
- [Full Javascript Course - 9 Hrs](https://www.youtube.com/watch?v=9M4XKi25I2M)
