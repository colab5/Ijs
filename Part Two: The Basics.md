# Intro
Hello there! welcome to __Intro to JS__
to begin we'll start with the obvious. Comments!

to comment in JS you want to either use // or /* for multi-line comments */  

# Basics

## Console Entries
Now let's move on to general console alerts to put things in to the console or get a response you want to put console.(and here goes what type of message it is)

There is one type console.log  the .log justs put the text into the terminal out put so if I put,
```js
console.log("Hello, World");
```
 the output would be "Hello, World"
 you can try this yourself by pressing CTRL + SHIFT + I  and clicking on the console tab, and typing this in!


## Variables

 Like almost every dynamic language, JavaScript is a "duck-typed" language, and therefore every variable is defined using the var keyword, and can contain all types of variables.

We can define several types of variables to use in our code:

```js
var MyNumber = 12; //A number - In JavaScript, the Number type can be both a floating point number and an integer.
var MyString = "Wassup!"; //A String 
var MyBoolean = true; // A boolean - Boolean variables can only be equal to either true or false.
```
When a variable is used without first defining a value for it, it is equal to undefined. For example:
```js
var test;
console.log(test);
```
the output is undefined

However, the null value is a different type of value, and is used when a variable should be marked as empty. undefined can be used for this purpose, but it should not be used. 

example: 

```js

var test = null;
console.log(test);

//output = null
```
### Sidenote

// to combine to strings just use the + or , symbol ex:
```js
var test = "sup bro how old are you?";
var age = 20;
console.log(test, age);
```
the output is "sup bro how old are you? 20"

but if I do
```js
console.log(test + age)
```

The output becomes: "sup bro how old are you?20"

Learn more on this here: [Addition](https://gitlab.com/colab5/Ijs/-/blob/master/Part%20Two:%20The%20Basics.md#addition)

## Arrays:

JavaScript can hold an array of variables in an Array object. In JavaScript, an array also functions as a list, a stack or a queue.

To define an array, either use the brackets notation or the Array object notation: 
```js
var MyArray = [1,2,3];
var SameArray = new Array(1,2,3);
```

### Addressing

We can use the brackets `[]` operator to address a specific cell in our array. Addressing uses zero-based indices, so for example, in `myArray` the 2nd member can be addressed with index 1. One of the benefits of using an array datastructure is that you have constant time look-up, if you already know the index of the element you are trying to access.
```js
console.log(myArray[1]);      
// output: 2
```


Arrays in JavaScript are sparse, meaning that we can also assign variables to random locations even though previous cells were undefined. For example:
```js
var MyArray = [ ];
myArray[3] = "hello"
console.log(myArray)
// output: [undefined, undefined, undefined, "hello"]
```
# Operators
## Addition

The `+` (addition) operator is used for both addition and concatenation of strings.

For example, adding two variables is easy:

```js
var a = 1;
var b = 2;
var c = a + b;     // c is now equal to 3
```

The addition operator is used for concatenating strings to strings, strings to numbers, and numbers to strings:

```js
var name = "John";
console.log("Hello " + name + "!");
console.log("The meaning of life is " + 42);
console.log(42 + " is the meaning of life");
```



JavaScript behaves differently when you are trying to combine two operands of different types. The default primitive value is a string, so when you try to add a number to a string, JavaScript will transform the number to a string before the concatenation
 
## Follow up Projects
